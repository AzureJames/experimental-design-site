const rowDirection = function (evt) {
	if (evt.target == document.querySelector('button.reverse'))
		document.querySelector('div.flex').style.flexDirection = "row-reverse"; 
	else 
		document.querySelector('div.flex').style.flexDirection = "row"; 
}

document
	.querySelector('.reverse')
	.addEventListener('click', rowDirection);

document
	.querySelector('.standard-row')
	.addEventListener('click', rowDirection);

